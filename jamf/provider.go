package jamf

import (
	"fmt"
	"net/url"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
	"gitlab.com/mjburling/go-jamf-sdk"
)

var jamfProvider *schema.Provider

func Provider() terraform.ResourceProvider {
	jamfProvider = &schema.Provider{
		Schema: map[string]*schema.Schema{
			"password": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.MultiEnvDefaultFunc([]string{"JAMF_PASSWORD", "JSS_PASSWORD"}, nil),
			},
			"username": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.MultiEnvDefaultFunc([]string{"JAMF_USERNAME", "JSS_USERNAME"}, nil),
			},
			"baseurl": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.MultiEnvDefaultFunc([]string{"JAMF_BASEURL", "JSS_BASEURL"}, nil),
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"jamf_script":   resourceJamfScript(),
			"jamf_category": resourceJamfCategory(),
		},
		ConfigureFunc: providerConfigure,
	}

	return jamfProvider
}

type ProviderConfiguration struct {
	JamfClient *jamf.Client
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	username := d.Get("username").(string)
	password := d.Get("password").(string)
	baseurl := d.Get("baseurl").(string)

	jamfClient := jamf.NewClient(username, password, baseurl)

	return &ProviderConfiguration{
		JamfClient: jamfClient,
	}, nil
}

// translateClientError translates the specific Jamf Client errors
// into errors consumable by terraform
func translateClientError(err error, msg string) error {
	if msg == "" {
		msg = "an error occurred"
	}

	if errURL, ok := err.(*url.Error); ok {
		return fmt.Errorf(msg+" (url.Error): %s", errURL)
	}

	return fmt.Errorf(msg+": %s", err.Error())
}
