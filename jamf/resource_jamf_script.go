package jamf

import (
	"fmt"
	"strconv"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"gitlab.com/mjburling/go-jamf-sdk"
)

func resourceJamfScript() *schema.Resource {
	return &schema.Resource{
		Create: resourceJamfScriptCreate,
		Delete: resourceJamfScriptDelete,
		Read:   resourceJamfScriptRead,
		Update: resourceJamfScriptUpdate,
		Importer: &schema.ResourceImporter{
			State: resourceJamfScriptImport,
		},

		Schema: map[string]*schema.Schema{
			"category": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Default:  "No category assigned",
			},
			"filename": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"info": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"notes": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"os_requirements": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"priority": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Default:  "After",
			},
			"script_contents": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
		},
	}
}

func buildJamfScript(d *schema.ResourceData) (*jamf.Script, error) {
	var id int
	if d.Id() != "" {
		var err error
		id, err = strconv.Atoi(d.Id())
		if err != nil {
			return nil, err
		}
	}
	script := &jamf.Script{
		ID: jamf.Int(id),
	}

	if v, ok := d.GetOk("name"); ok {
		script.Name = jamf.String(v.(string))
	}

	if v, ok := d.GetOk("category"); ok {
		script.Category = jamf.String(v.(string))
	}

	if v, ok := d.GetOk("filename"); ok {
		if v == "" {
			script.Filename = script.Name
		} else {
			script.Filename = jamf.String(v.(string))
		}
	}

	if v, ok := d.GetOk("info"); ok {
		script.Info = jamf.String(v.(string))
	}

	if v, ok := d.GetOk("notes"); ok {
		script.Notes = jamf.String(v.(string))
	}

	if v, ok := d.GetOk("priority"); ok {
		script.Priority = jamf.String(v.(string))
	}

	if v, ok := d.GetOk("os_requirements"); ok {
		script.OSRequirements = jamf.String(v.(string))
	}

	if v, ok := d.GetOk("script_contents"); ok {
		script.ScriptContents = jamf.String(v.(string))
	}

	return script, nil
}

func resourceJamfScriptRead(d *schema.ResourceData, meta interface{}) error {
	providerConf := meta.(*ProviderConfiguration)
	client := providerConf.JamfClient
	id, err := strconv.Atoi(d.Id())
	if err != nil {
		return err
	}
	script, err := client.GetScript(id)
	if err != nil {
		d.SetId("")
		return nil
	}
	if script.Category != nil {
		if err := d.Set("category", script.GetCategory()); err != nil {
			return err
		}
	}
	if script.Filename != nil {
		if err := d.Set("filename", script.GetFilename()); err != nil {
			return err
		}
	} else {
		if err := d.Set("filename", script.GetName()); err != nil {
			return err
		}
	}
	if script.Info != nil {
		if err := d.Set("info", script.GetInfo()); err != nil {
			return err
		}
	}
	if script.Name != nil {
		if err := d.Set("name", script.GetName()); err != nil {
			return err
		}
	}
	if script.OSRequirements != nil {
		if err := d.Set("os_requirements", script.GetOSRequirements()); err != nil {
			return err
		}
	}
	if script.Priority != nil {
		if err := d.Set("priority", script.GetPriority()); err != nil {
			return err
		}
	}
	if script.ScriptContents != nil {
		if err := d.Set("script_contents", script.GetScriptContents()); err != nil {
			return err
		}
	}

	d.SetId(strconv.Itoa(script.GetID()))

	return nil
}

func resourceJamfScriptCreate(d *schema.ResourceData, meta interface{}) error {
	providerConf := meta.(*ProviderConfiguration)
	client := providerConf.JamfClient
	scriptPayload, err := buildJamfScript(d)
	if err != nil {
		return fmt.Errorf("failed to parse resource configuration: %s", err.Error())
	}
	script, err := client.CreateScript(scriptPayload)
	if err != nil {
		return translateClientError(err, "error creating script")
	}
	id := strconv.Itoa(script.GetID())

	d.SetId(id)

	return resourceJamfScriptRead(d, meta)
}

func resourceJamfScriptUpdate(d *schema.ResourceData, meta interface{}) error {
	providerConf := meta.(*ProviderConfiguration)
	client := providerConf.JamfClient
	scriptPayload, err := buildJamfScript(d)
	if err != nil {
		return fmt.Errorf("failed to parse resource configuration: %s", err.Error())
	}
	err = client.UpdateScript(scriptPayload)

	if err != nil {
		return translateClientError(err, "error creating script")
	}

	return resourceJamfScriptRead(d, meta)
}

func resourceJamfScriptDelete(d *schema.ResourceData, meta interface{}) error {
	id, err := strconv.Atoi(d.Id())
	if err != nil {
		return err
	}
	providerConf := meta.(*ProviderConfiguration)
	client := providerConf.JamfClient
	if err = client.DeleteScript(id); err != nil {
		return translateClientError(err, "error deleting script")
	}

	return nil
}

func resourceJamfScriptImport(d *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
	if err := resourceJamfScriptRead(d, meta); err != nil {
		return nil, err
	}
	return []*schema.ResourceData{d}, nil
}
