package jamf

import (
	"fmt"
	"strconv"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"gitlab.com/mjburling/go-jamf-sdk"
)

func resourceJamfCategory() *schema.Resource {
	return &schema.Resource{
		Create: resourceJamfCategoryCreate,
		Delete: resourceJamfCategoryDelete,
		Read:   resourceJamfCategoryRead,
		Update: resourceJamfCategoryUpdate,
		Importer: &schema.ResourceImporter{
			State: resourceJamfCategoryImport,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"priority": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Default:  "9",
			},
		},
	}
}

func buildJamfCategory(d *schema.ResourceData) (*jamf.Category, error) {
	var id int
	if d.Id() != "" {
		var err error
		id, err = strconv.Atoi(d.Id())
		if err != nil {
			return nil, err
		}
	}
	category := &jamf.Category{
		ID: jamf.Int(id),
	}

	if v, ok := d.GetOk("name"); ok {
		category.Name = jamf.String(v.(string))
	}

	if v, ok := d.GetOk("priority"); ok {
		category.Priority = jamf.String(v.(string))
	}

	return category, nil
}

func resourceJamfCategoryRead(d *schema.ResourceData, meta interface{}) error {
	providerConf := meta.(*ProviderConfiguration)
	client := providerConf.JamfClient
	id, err := strconv.Atoi(d.Id())
	if err != nil {
		return err
	}
	category, err := client.GetCategory(id)
	if err != nil {
		d.SetId("")
		return nil
	}
	if category.Name != nil {
		if err := d.Set("name", category.GetName()); err != nil {
			return err
		}
	}
	if category.Priority != nil {
		if err := d.Set("priority", category.GetPriority()); err != nil {
			return err
		}
	}

	d.SetId(strconv.Itoa(category.GetID()))

	return nil
}

func resourceJamfCategoryCreate(d *schema.ResourceData, meta interface{}) error {
	providerConf := meta.(*ProviderConfiguration)
	client := providerConf.JamfClient
	categoryPayload, err := buildJamfCategory(d)
	if err != nil {
		return fmt.Errorf("failed to parse resource configuration: %s", err.Error())
	}
	category, err := client.CreateCategory(categoryPayload)
	if err != nil {
		return translateClientError(err, "error creating category")
	}
	id := strconv.Itoa(category.GetID())

	d.SetId(id)

	return resourceJamfCategoryRead(d, meta)
}

func resourceJamfCategoryUpdate(d *schema.ResourceData, meta interface{}) error {
	providerConf := meta.(*ProviderConfiguration)
	client := providerConf.JamfClient
	categoryPayload, err := buildJamfCategory(d)
	if err != nil {
		return fmt.Errorf("failed to parse resource configuration: %s", err.Error())
	}
	err = client.UpdateCategory(categoryPayload)

	if err != nil {
		return translateClientError(err, "error creating category")
	}

	return resourceJamfCategoryRead(d, meta)
}

func resourceJamfCategoryDelete(d *schema.ResourceData, meta interface{}) error {
	id, err := strconv.Atoi(d.Id())
	if err != nil {
		return err
	}
	providerConf := meta.(*ProviderConfiguration)
	client := providerConf.JamfClient
	if err = client.DeleteCategory(id); err != nil {
		return translateClientError(err, "error deleting category")
	}

	return nil
}

func resourceJamfCategoryImport(d *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
	if err := resourceJamfCategoryRead(d, meta); err != nil {
		return nil, err
	}
	return []*schema.ResourceData{d}, nil
}
