module gitlab.com/mjburling/terraform-provider-jamf

go 1.14

require (
	github.com/hashicorp/terraform-plugin-sdk v1.13.0
	gitlab.com/mjburling/go-jamf-sdk v0.1.0
)
