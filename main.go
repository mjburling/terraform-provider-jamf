package main

import (
	"github.com/hashicorp/terraform-plugin-sdk/plugin"
	"gitlab.com/mjburling/terraform-provider-jamf/jamf"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: jamf.Provider})
}
